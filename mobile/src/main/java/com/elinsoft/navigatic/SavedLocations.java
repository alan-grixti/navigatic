package com.elinsoft.navigatic;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.elinsoft.navigaticmanagers.locations_manager.Location;
import com.elinsoft.navigaticmanagers.locations_manager.LocationsManager;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class SavedLocations extends AppCompatActivity {

    //#region Properties

    /**
     * Reference to the Locations Manager
     */
    private LocationsManager locationsManager = LocationsManager.getInstance();

    //#endregion


    //#region Methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_locations);

        // get the saved locations
        Location[] savedLocations = this.locationsManager.getSavedLocations();

        // get reference to the ListView
        ListView savedLocationsListView = (ListView) findViewById(R.id.savedLocationsList);

        // create the adapter object
        LocationsAdapter adapter = new LocationsAdapter(this, savedLocations);

        // set the adapter of the ListView
        savedLocationsListView.setAdapter(adapter);
    }

    //#endregion


    /**
     * Location adapter class implementation
     */
    private class LocationsAdapter extends ArrayAdapter<Location>{

        /**
         * Class constructor
         * @param context
         * @param locations
         */
        public LocationsAdapter(Context context, Location[] locations){
            // chain to super constructor
            super(context, 0, locations);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            // get the data item to render
            Location location = getItem(position);

            // check if the view is being reused, otherwise inflate
            if(convertView == null)
            {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.saved_location_item,parent, false);
            }

            // find the views from the layout and bind the data
            TextView tvName = (TextView) convertView.findViewById(R.id.saved_location_name);
            TextView tvAddress = (TextView) convertView.findViewById(R.id.saved_location_address);

            tvName.setText(location.getLocationName());
            tvAddress.setText(location.getLocationAddress());

            return convertView;
        }
    }
}
