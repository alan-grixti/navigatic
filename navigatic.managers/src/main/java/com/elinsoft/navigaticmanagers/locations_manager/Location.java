package com.elinsoft.navigaticmanagers.locations_manager;

/**
 * Created by Alan on 3/10/2018.
 */

public class Location {

    //#region Constructors

    /**
     * Class constructor
     * @param locationName name of the location
     * @param locationAddress address of the location
     * @param locationLng longitude of the location
     * @param locationLat latitude of the location
     */
    public Location(String locationName, String locationAddress, double locationLng, double locationLat){
        this.locationName = locationName;
        this.locationAddress = locationAddress;
        this.locationLng = locationLng;
        this.locationLat = locationLat;
    }

    //#endregion


    //#region Properties

    /**
     * Name of the saved location
     */
    private String locationName;

    /**
     * Address of the saved location
     */
    private String locationAddress;

    /**
     * Longitude of the saved location
     */
    private double locationLng;

    /**
     * Latitude of the saved location
     */
    private double locationLat;

    //#endregion


    //#region Methods

    //#region Getters and Setters

    /**
     * Name of the saved location
     */
    public String getLocationName() {
        return locationName;
    }

    /**
     * Setter for the location's name
     * @param locationName
     */
    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    /**
     * Address of the saved location
     */
    public String getLocationAddress() {
        return locationAddress;
    }

    /**
     * Setter for the saved location's address
     * @param locationAddress
     */
    public void setLocationAddress(String locationAddress) {
        this.locationAddress = locationAddress;
    }

    /**
     * Longitude of the saved location
     */
    public double getLocationLng() {
        return locationLng;
    }

    /**
     * Setter for the saved location's longitude
     * @param locationLng
     */
    public void setLocationLng(double locationLng) {
        this.locationLng = locationLng;
    }

    /**
     * Latitude of the saved location
     */
    public double getLocationLat() {
        return locationLat;
    }

    /**
     * Setter for the saved location's latitude
     * @param locationLat
     */
    public void setLocationLat(double locationLat) {
        this.locationLat = locationLat;
    }

    //#endregion

    @Override
    public String toString(){
        return this.getLocationName();
    }
    //#endregion



}
