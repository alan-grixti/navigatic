package com.elinsoft.navigaticmanagers.locations_manager;

import java.util.ArrayList;

/**
 * Created by Alan on 3/10/2018.
 */

public class LocationsManager {

    //#region Constructors

    /**
     * Class constructor
     */
    private LocationsManager(){
        this._locations = this.loadSavedLocations();
    }

    //#endregion


    //#region Properties

    /**
     * Singleton instance for the LocationsManager
     */
    private static LocationsManager _instance;

    /**
     * List of saved locations
     */
    private ArrayList<Location> _locations;

    //#endregion


    //#region Methods

    /**
     * Singleton instance accessor for the LocationsManager
     * @return The instance for the LocationsManager
     */
    public static LocationsManager getInstance(){
        if(_instance == null){
            _instance = new LocationsManager();
        }

        return _instance;
    }


    //#region Saving and Loading Locations

    /**
     * Load the user's saved locations
     * @return
     */
    private ArrayList<Location> loadSavedLocations(){

        ArrayList<Location> loadedLocations = new ArrayList<Location>();

        for(int i = 0; i < 10; i++){
            loadedLocations.add(new Location("Location "+i, "Triq il-qirda numru" + i,  14.57137-(0.1*i),35.86122-(0.1*i)));
        }

        return loadedLocations;
    }

    /**
     * Save a location
     * @param location
     */
    private void saveLocation(Location location){
        this._locations.add(location);

        //TODO serialize and save the location
    }

    /**
     * Get list of locations
     * @return
     */
    public Location[] getSavedLocations(){
        return this._locations.toArray(new Location[this._locations.size()]);
    }

    //#endregion
}
